#ifndef _SRESTORE_PRECOMPILED_H_
#define _SRESTORE_PRECOMPILED_H_

#define WIN32_LEAN_AND_MEAN

#include <string>
#include <vector>
#include <windows.h>
#include <accctrl.h>
#include <aclapi.h>
#include <objbase.h>
#include <strsafe.h>
#include <srrestoreptapi.h>

#pragma comment(lib, "ole32.lib")
#pragma comment(lib, "advapi32.lib")

typedef BOOL(WINAPI *PFN_SETRESTOREPTW) (PRESTOREPOINTINFOW, PSTATEMGRSTATUS);

#endif // !_SRESTORE_PRECOMPILED_H_