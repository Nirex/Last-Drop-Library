#ifndef _PROCESS_PRECOMPILED_H_
#define _PROCESS_PRECOMPILED_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <algorithm>
#include <vector>
#include <process.h>
#include <Tlhelp32.h>
#include <winbase.h>
#include <string.h>

#endif // !_PROCESS_PRECOMPILED_H_