#include "SRestore.h"

typedef BOOL(WINAPI *PFN_SETRESTOREPTW) (PRESTOREPOINTINFOW, PSTATEMGRSTATUS);

DWORD SRestore::Create(std::wstring szSrName, OUT INT64* iSeqNum)
{
	RESTOREPOINTINFOW RestorePtInfo;
	STATEMGRSTATUS SMgrStatus;
	PFN_SETRESTOREPTW fnSRSetRestorePointW = NULL;
	DWORD dwErr = ERROR_SUCCESS;
	HMODULE hSrClient = NULL;
	BOOL fRet = FALSE;
	HRESULT hr = S_OK;

	hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (FAILED(hr))
	{
		if (hSrClient != NULL)
		{
			FreeLibrary(hSrClient);
			hSrClient = NULL;
		}

		return 0;
	}

	fRet = InitializeCOMSecurity();
	if (!fRet)
	{
		if (hSrClient != NULL)
		{
			FreeLibrary(hSrClient);
			hSrClient = NULL;
		}

		return 0;
	}

	RestorePtInfo.dwEventType = BEGIN_SYSTEM_CHANGE;
	RestorePtInfo.dwRestorePtType = APPLICATION_INSTALL;
	RestorePtInfo.llSequenceNumber = 0;
	StringCbCopyW(RestorePtInfo.szDescription, sizeof(RestorePtInfo.szDescription), szSrName.c_str());

	hSrClient = LoadLibraryW(L"srclient.dll");
	if (hSrClient == NULL)
	{
		if (hSrClient != NULL)
		{
			FreeLibrary(hSrClient);
			hSrClient = NULL;
		}

		return 0;
	}

	fnSRSetRestorePointW = (PFN_SETRESTOREPTW)GetProcAddress(hSrClient, "SRSetRestorePointW");
	if (fnSRSetRestorePointW == NULL)
	{
		if (hSrClient != NULL)
		{
			FreeLibrary(hSrClient);
			hSrClient = NULL;
		}

		return 0;
	}

	fRet = fnSRSetRestorePointW(&RestorePtInfo, &SMgrStatus);
	if (!fRet)
	{
		dwErr = SMgrStatus.nStatus;
		if (dwErr == ERROR_SERVICE_DISABLED)
		{
			if (hSrClient != NULL)
			{
				FreeLibrary(hSrClient);
				hSrClient = NULL;
			}

			return 0;
		}
		if (hSrClient != NULL)
		{
			FreeLibrary(hSrClient);
			hSrClient = NULL;
		}

		return 0;
	}

	RestorePtInfo.dwEventType = END_SYSTEM_CHANGE;
	RestorePtInfo.llSequenceNumber = SMgrStatus.llSequenceNumber;
	*iSeqNum = SMgrStatus.llSequenceNumber;

	fRet = fnSRSetRestorePointW(&RestorePtInfo, &SMgrStatus);
	if (!fRet)
	{
		dwErr = SMgrStatus.nStatus;
		if (hSrClient != NULL)
		{
			FreeLibrary(hSrClient);
			hSrClient = NULL;
		}

		return 0;
	}

	if (hSrClient != NULL)
	{
		FreeLibrary(hSrClient);
		hSrClient = NULL;
	}

	return 0;
}

DWORD SRestore::Remove(DWORD iSeqNum)
{
	typedef DWORD (CALLBACK* SRemove)(INT64);
	DWORD retRes = NULL;
	HINSTANCE srHandle = LoadLibrary(L"SrClient.dll");
	SRemove srRemoveFPtr = (SRemove)GetProcAddress(srHandle, "SRRemoveRestorePoint");

	if (srRemoveFPtr != NULL)
	{
		retRes = srRemoveFPtr(iSeqNum);
	}

	FreeLibrary(srHandle);
	return retRes;
}

DWORD SRestore::RemoveInRange(INT64 begin, INT64 end, OUT INT* iDeletedCount)
{
	DWORD retVal;
	
	for (INT64 i = begin; i < end; i++)
	{
		retVal = Remove((DWORD)i);
		if (retVal == ERROR_SUCCESS)
		{
			*iDeletedCount++;
		}
	}
	return retVal;
}

BOOL SRestore::InitializeCOMSecurity(void)
{
	SECURITY_DESCRIPTOR securityDesc = { 0 };
	EXPLICIT_ACCESS   ea[5] = { 0 };
	ACL        *pAcl = NULL;
	ULONGLONG  rgSidBA[(SECURITY_MAX_SID_SIZE + sizeof(ULONGLONG) - 1) / sizeof(ULONGLONG)] = { 0 };
	ULONGLONG  rgSidLS[(SECURITY_MAX_SID_SIZE + sizeof(ULONGLONG) - 1) / sizeof(ULONGLONG)] = { 0 };
	ULONGLONG  rgSidNS[(SECURITY_MAX_SID_SIZE + sizeof(ULONGLONG) - 1) / sizeof(ULONGLONG)] = { 0 };
	ULONGLONG  rgSidPS[(SECURITY_MAX_SID_SIZE + sizeof(ULONGLONG) - 1) / sizeof(ULONGLONG)] = { 0 };
	ULONGLONG  rgSidSY[(SECURITY_MAX_SID_SIZE + sizeof(ULONGLONG) - 1) / sizeof(ULONGLONG)] = { 0 };
	DWORD      cbSid = 0;
	BOOL       fRet = FALSE;
	DWORD      dwRet = ERROR_SUCCESS;
	HRESULT    hrRet = S_OK;

	fRet = InitializeSecurityDescriptor(&securityDesc, SECURITY_DESCRIPTOR_REVISION);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	cbSid = sizeof(rgSidBA);
	fRet = CreateWellKnownSid(WinBuiltinAdministratorsSid, NULL, rgSidBA, &cbSid);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	cbSid = sizeof(rgSidLS);
	fRet = CreateWellKnownSid(WinLocalServiceSid, NULL, rgSidLS, &cbSid);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	cbSid = sizeof(rgSidNS);
	fRet = CreateWellKnownSid(WinNetworkServiceSid, NULL, rgSidNS, &cbSid);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	cbSid = sizeof(rgSidPS);
	fRet = CreateWellKnownSid(WinSelfSid, NULL, rgSidPS, &cbSid);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	cbSid = sizeof(rgSidSY);
	fRet = CreateWellKnownSid(WinLocalSystemSid, NULL, rgSidSY, &cbSid);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	ea[0].grfAccessPermissions = COM_RIGHTS_EXECUTE | COM_RIGHTS_EXECUTE_LOCAL;
	ea[0].grfAccessMode = SET_ACCESS;
	ea[0].grfInheritance = NO_INHERITANCE;
	ea[0].Trustee.pMultipleTrustee = NULL;
	ea[0].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[0].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[0].Trustee.ptstrName = (LPTSTR)rgSidBA;

	ea[1].grfAccessPermissions = COM_RIGHTS_EXECUTE | COM_RIGHTS_EXECUTE_LOCAL;
	ea[1].grfAccessMode = SET_ACCESS;
	ea[1].grfInheritance = NO_INHERITANCE;
	ea[1].Trustee.pMultipleTrustee = NULL;
	ea[1].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[1].Trustee.ptstrName = (LPTSTR)rgSidLS;

	ea[2].grfAccessPermissions = COM_RIGHTS_EXECUTE | COM_RIGHTS_EXECUTE_LOCAL;
	ea[2].grfAccessMode = SET_ACCESS;
	ea[2].grfInheritance = NO_INHERITANCE;
	ea[2].Trustee.pMultipleTrustee = NULL;
	ea[2].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	ea[2].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[2].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[2].Trustee.ptstrName = (LPTSTR)rgSidNS;

	ea[3].grfAccessPermissions = COM_RIGHTS_EXECUTE | COM_RIGHTS_EXECUTE_LOCAL;
	ea[3].grfAccessMode = SET_ACCESS;
	ea[3].grfInheritance = NO_INHERITANCE;
	ea[3].Trustee.pMultipleTrustee = NULL;
	ea[3].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	ea[3].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[3].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[3].Trustee.ptstrName = (LPTSTR)rgSidPS;

	ea[4].grfAccessPermissions = COM_RIGHTS_EXECUTE | COM_RIGHTS_EXECUTE_LOCAL;
	ea[4].grfAccessMode = SET_ACCESS;
	ea[4].grfInheritance = NO_INHERITANCE;
	ea[4].Trustee.pMultipleTrustee = NULL;
	ea[4].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	ea[4].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[4].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[4].Trustee.ptstrName = (LPTSTR)rgSidSY;

	dwRet = SetEntriesInAcl(ARRAYSIZE(ea), ea, NULL, &pAcl);
	if (dwRet != ERROR_SUCCESS || pAcl == NULL)
	{
		fRet = FALSE;
		LocalFree(pAcl);
		return fRet;
	}

	fRet = SetSecurityDescriptorOwner(&securityDesc, rgSidBA, FALSE);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	fRet = SetSecurityDescriptorGroup(&securityDesc, rgSidBA, FALSE);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	fRet = SetSecurityDescriptorDacl(&securityDesc, TRUE, pAcl, FALSE);
	if (!fRet)
	{
		LocalFree(pAcl);
		return fRet;
	}

	hrRet = CoInitializeSecurity(&securityDesc, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_PKT_PRIVACY, RPC_C_IMP_LEVEL_IDENTIFY, NULL, EOAC_DISABLE_AAA | EOAC_NO_CUSTOM_MARSHAL, NULL);
	if (FAILED(hrRet))
	{
		fRet = FALSE;	
		LocalFree(pAcl);
		return fRet;

	}

	fRet = TRUE;
	LocalFree(pAcl);
	return fRet;
}
