#ifndef _FIREWALL_PRECOMPILED_H_
#define _FIREWALL_PRECOMPILED_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <crtdbg.h>
#include <netfw.h>
#include <objbase.h>
#include <oleauto.h>
#include <stdio.h>

#pragma comment( lib, "ole32.lib" )
#pragma comment( lib, "oleaut32.lib" )

#endif // !_FIREWALL_PRECOMPILED_H_
