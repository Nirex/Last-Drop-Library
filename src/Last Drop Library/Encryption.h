#ifndef _ENCRYPTION_H_
#define _ENCRYPTION_H_

#include "GeneralPrecompiled.h"

class Encryption
{
PUBLIC:
	static const unsigned char* Encrypt(const unsigned char* bytes, const unsigned char* key);
	static const unsigned char* Decrypt(const unsigned char* bytes, const unsigned char* key);

};

#endif // !_ENCRYPTION_H_
