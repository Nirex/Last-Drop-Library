#ifndef _GENERAL_PRECOMPILED_H_
#define _GENERAL_PRECOMPILED_H_

#define LDL
#define DDL

#define PUBLIC public
#define PROTECTED protected
#define PRIVATE private

#define PUBLIC_AUXILARY public
#define PROTECTED_AUXILARY protected
#define PRIVATE_AUXILARY private

#define VIRTUAL public
#define PURE_VIRTUAL protected

#endif // !_GENERAL_PRECOMPILED_H_