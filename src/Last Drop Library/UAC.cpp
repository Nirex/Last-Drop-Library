#include "UAC.h"

UAC::UAC(void)
{
	m_reg = new Registry();
}

UAC::~UAC(void)
{
	delete m_reg;
}

BOOL UAC::Enable(OUT REGVAL* rvpResult)
{
	REGVAL RHold(0, 0);
	if (m_reg->Write(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", L"EnableLUA", REG_DWORD, L"1", &RHold) == 0)
	{
		if (rvpResult != nullptr)
		{
			rvpResult->length = RHold.length;
			rvpResult->value = RHold.value;
		}
		return 0;
	}
	return 1;
}

BOOL UAC::Disable(OUT REGVAL* rvpResult)
{
	REGVAL RHold(0, 0);
	if (m_reg->Write(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", L"EnableLUA", REG_DWORD, L"0", &RHold) == 0)
	{
		if (rvpResult != nullptr)
		{
			rvpResult->length = RHold.length;
			rvpResult->value = RHold.value;
		}
		return 0;
	}
	return 1;
}
