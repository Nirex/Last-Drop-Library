#ifndef _SRESTORE_H_
#define _SRESTORE_H_

#include "GeneralPrecompiled.h"
#include "SRestorePrecompiled.h"

class SRestore
{
PUBLIC:
	DWORD Create(std::wstring szSrName, OUT INT64* iSeqNum);
	DWORD Remove(DWORD iSeqNum);
	DWORD RemoveInRange(INT64 begin, INT64 end, OUT INT* iDeletedCount);

PRIVATE:
	BOOL InitializeCOMSecurity(void);
};

#endif // !_SRESTORE_H_
