#ifndef _SERVICE_PRECOMPILED_H_
#define _SERVICE_PRECOMPILED_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <string>
#include <vector>
#include <tchar.h>
#include <strsafe.h>
#include <aclapi.h>

#pragma comment(lib, "advapi32.lib")

#endif // !_SERVICE_PRECOMPILED_H_