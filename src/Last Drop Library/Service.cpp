#include "Service.h"

BOOL Service::Start(const std::wstring& szName)
{
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (NULL == SCManager)
	{
		return 1;
	}
	if (SHandle == NULL)
	{
		return 1;
	}
	if (!StartService(SHandle, 0, NULL))
	{
		return 1;
	}

	CloseServiceHandle(SCManager);
	CloseServiceHandle(SHandle);

	return 0;
}

BOOL Service::Stop(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);
	
	if (SCManager == NULL)
	{
		return 1;
	}
	if (SHandle == NULL)
	{
		CloseServiceHandle(SCManager);
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SCManager);
		CloseServiceHandle(SHandle);
		return 1;
	}

	CloseServiceHandle(SCManager);
	CloseServiceHandle(SHandle);
	return 0;
}

BOOL Service::Restart(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (SHandle == NULL)
	{
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SHandle);
		return 1;
	}
	do
	{
		QueryServiceStatus(SHandle, &Status);
	} while (Status.dwCurrentState != SERVICE_STOPPED);

	if (!StartService(SHandle, 0, NULL))
	{
		CloseServiceHandle(SCManager);
		CloseServiceHandle(SHandle);
		return 0;
	}
}

BOOL Service::Disable(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (SHandle == NULL)
	{
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SHandle);
		return 1;
	}

	std::wstring szPath;
	GetBinaryPath(szName, &szPath);

	std::wstring szLoadOrder;
	GetLoadOrderGroup(szName, &szLoadOrder);

	DWORD dwTagID;
	GetTagID(szName, &dwTagID);

	std::wstring szDependencies;
	GetDependencies(szName, &szDependencies);

	if (ChangeServiceConfig(SHandle, SERVICE_NO_CHANGE, SERVICE_DISABLED, SERVICE_NO_CHANGE, szPath.c_str(), szLoadOrder.c_str(), &dwTagID, szDependencies.c_str(), NULL, NULL, NULL))
	{
		return 0;
	}

	CloseServiceHandle(SHandle);
	CloseServiceHandle(SCManager);
	return 1;
}

BOOL Service::ManualStart(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (SHandle == NULL)
	{
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SHandle);
		return 1;
	}

	std::wstring szPath;
	GetBinaryPath(szName, &szPath);

	std::wstring szLoadOrder;
	GetLoadOrderGroup(szName, &szLoadOrder);

	DWORD dwTagID;
	GetTagID(szName, &dwTagID);

	std::wstring szDependencies;
	GetDependencies(szName, &szDependencies);

	if (ChangeServiceConfig(SHandle, SERVICE_NO_CHANGE, SERVICE_DEMAND_START, SERVICE_NO_CHANGE, szPath.c_str(), szLoadOrder.c_str(), &dwTagID, szDependencies.c_str(), NULL, NULL, NULL))
	{
		return 0;
	}

	CloseServiceHandle(SHandle);
	CloseServiceHandle(SCManager);
	return 1;
}

BOOL Service::AutoStart(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (SHandle == NULL)
	{
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SHandle);
		return 1;
	}

	std::wstring szPath;
	GetBinaryPath(szName, &szPath);

	std::wstring szLoadOrder;
	GetLoadOrderGroup(szName, &szLoadOrder);

	DWORD dwTagID;
	GetTagID(szName, &dwTagID);

	std::wstring szDependencies;
	GetDependencies(szName, &szDependencies);

	if (ChangeServiceConfig(SHandle, SERVICE_NO_CHANGE, SERVICE_AUTO_START, SERVICE_NO_CHANGE, szPath.c_str(), szLoadOrder.c_str(), &dwTagID, szDependencies.c_str(), NULL, NULL, NULL))
	{
		return 0;
	}

	CloseServiceHandle(SHandle);
	CloseServiceHandle(SCManager);
	return 1;
}

BOOL Service::BootStart(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (SHandle == NULL)
	{
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SHandle);
		return 1;
	}

	std::wstring szPath;
	GetBinaryPath(szName, &szPath);

	std::wstring szLoadOrder;
	GetLoadOrderGroup(szName, &szLoadOrder);

	DWORD dwTagID;
	GetTagID(szName, &dwTagID);

	std::wstring szDependencies;
	GetDependencies(szName, &szDependencies);

	if (ChangeServiceConfig(SHandle, SERVICE_NO_CHANGE, SERVICE_BOOT_START, SERVICE_NO_CHANGE, szPath.c_str(), szLoadOrder.c_str(), &dwTagID, szDependencies.c_str(), NULL, NULL, NULL))
	{
		return 0;
	}

	CloseServiceHandle(SHandle);
	CloseServiceHandle(SCManager);
	return 1;
}

BOOL Service::SystemStart(const std::wstring& szName)
{
	SERVICE_STATUS Status;
	SC_HANDLE SCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	SC_HANDLE SHandle = OpenService(SCManager, szName.c_str(), SC_MANAGER_ALL_ACCESS);

	if (SHandle == NULL)
	{
		return 1;
	}
	if (!ControlService(SHandle, SERVICE_CONTROL_STOP, &Status))
	{
		CloseServiceHandle(SHandle);
		return 1;
	}

	std::wstring szPath;
	GetBinaryPath(szName, &szPath);

	std::wstring szLoadOrder;
	GetLoadOrderGroup(szName, &szLoadOrder);

	DWORD dwTagID;
	GetTagID(szName, &dwTagID);

	std::wstring szDependencies;
	GetDependencies(szName, &szDependencies);

	if (ChangeServiceConfig(SHandle, SERVICE_NO_CHANGE, SERVICE_SYSTEM_START, SERVICE_NO_CHANGE, szPath.c_str(), szLoadOrder.c_str(), &dwTagID, szDependencies.c_str(), NULL, NULL, NULL))
	{
		return 0;
	}

	CloseServiceHandle(SHandle);
	CloseServiceHandle(SCManager);
	return 1;
}

BOOL Service::GetType(const std::wstring& szName, OUT DWORD* dwType)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*dwType = lpsc->dwServiceType;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetStartType(const std::wstring& szName, OUT DWORD* dwStartType)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*dwStartType = lpsc->dwStartType;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetErrorControl(const std::wstring& szName, OUT DWORD* dwErrorControl)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*dwErrorControl = lpsc->dwErrorControl;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetBinaryPath(const std::wstring& szName, OUT std::wstring* szBinaryPath)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*szBinaryPath = lpsc->lpBinaryPathName;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetAccount(const std::wstring& szName, OUT std::wstring* szAccount)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*szAccount = lpsc->lpServiceStartName;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetDescription(const std::wstring& szName, OUT std::wstring* szDescription)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*szDescription = lpsd->lpDescription;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetLoadOrderGroup(const std::wstring& szName, OUT std::wstring* szLoadOrderGroup)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*szLoadOrderGroup = lpsc->lpLoadOrderGroup;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetDependencies(const std::wstring& szName, OUT std::wstring* szDependencies)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*szDependencies = lpsc->lpDependencies;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GetTagID(const std::wstring& szName, OUT DWORD* dwTagID)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	LPQUERY_SERVICE_CONFIG lpsc = NULL;
	LPSERVICE_DESCRIPTION lpsd = NULL;
	DWORD dwBytesNeeded, cbBufSize, dwError;

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		printf("OpenSCManager failed (%d)\n", GetLastError());
		return 1;
	}

	schService = OpenService(schSCManager, szName.c_str(), SERVICE_QUERY_CONFIG);
	if (schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		return 1;
	}

	if (!QueryServiceConfig(schService, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsc = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig(schService, lpsc, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &dwBytesNeeded))
	{
		dwError = GetLastError();
		if (ERROR_INSUFFICIENT_BUFFER == dwError)
		{
			cbBufSize = dwBytesNeeded;
			lpsd = (LPSERVICE_DESCRIPTION)LocalAlloc(LMEM_FIXED, cbBufSize);
		}
		else
		{
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
		}
	}

	if (!QueryServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)lpsd, cbBufSize, &dwBytesNeeded))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
	}

	*dwTagID = lpsc->dwTagId;

	LocalFree(lpsc);
	LocalFree(lpsd);

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

BOOL Service::GrabAll(OUT std::vector<std::wstring>* bServiceNames, OUT std::vector<std::wstring>* bDesplayNames)
{
	SC_HANDLE SHandle = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (NULL == SHandle)
	{
		return 1;
	}

	ENUM_SERVICE_STATUS service;
	DWORD dwBytesNeeded = 0;
	DWORD dwServicesReturned = 0;
	DWORD dwResumedHandle = 0;
	DWORD dwServiceType = SERVICE_WIN32 | SERVICE_DRIVER;

	BOOL retVal = EnumServicesStatus(SHandle, dwServiceType, SERVICE_STATE_ALL, &service, sizeof(ENUM_SERVICE_STATUS), &dwBytesNeeded, &dwServicesReturned, &dwResumedHandle);
	if (!retVal)
	{
		if (ERROR_MORE_DATA == GetLastError())
		{
			DWORD dwBytes = sizeof(ENUM_SERVICE_STATUS) + dwBytesNeeded;
			ENUM_SERVICE_STATUS* pServices = NULL;
			pServices = new ENUM_SERVICE_STATUS[dwBytes];

			EnumServicesStatus(SHandle, SERVICE_WIN32 | SERVICE_DRIVER, SERVICE_STATE_ALL, pServices, dwBytes, &dwBytesNeeded, &dwServicesReturned, &dwResumedHandle);

			for (unsigned iIndex = 0; iIndex < dwServicesReturned; iIndex++)
			{
				bServiceNames->push_back((pServices + iIndex)->lpServiceName);
				bDesplayNames->push_back((pServices + iIndex)->lpDisplayName);
			}
			delete[] pServices;
			pServices = NULL;
		}
	}
	CloseServiceHandle(SHandle);
	return 0;
}