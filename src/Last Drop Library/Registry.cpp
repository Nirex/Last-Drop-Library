#include "Registry.h"

BOOL Registry::Read(std::wstring szSubkey, std::wstring szName, DWORD type, OUT REGVAL* val)
{
	HKEY key;
	TCHAR value[255];
	DWORD valueLength = 255;
	INT RetVal = 0;

	RegOpenKeyEx(HKEY_LOCAL_MACHINE, szSubkey.c_str(), 0, (KEY_WRITE | KEY_READ), &key);
	if(RegQueryValueEx(key, szName.c_str(), NULL, &type, (LPBYTE)&value, &valueLength) == ERROR_SUCCESS)
	{
		RetVal = 0;
	}
	else
	{
		RetVal = 1;
	}

	RegCloseKey(key);

	if (val != nullptr)
	{
		val->length = valueLength;
		val->value = value;
	}

	return RetVal;
}

BOOL Registry::Write(std::wstring szSubkey, std::wstring szName, DWORD type, const wchar_t* value, OUT REGVAL* val)
{
	HKEY key;
	INT RetVal = 0;

	RegOpenKeyEx(HKEY_LOCAL_MACHINE, szSubkey.c_str(), 0, (KEY_WRITE | KEY_READ), &key);
	if(RegSetValueEx(key, szName.c_str(), 0, type, (LPBYTE)value, lstrlen(value) * sizeof(wchar_t)))
	{
		RetVal = 0;
	}
	else
	{
		RetVal = 1;
	}

	RegCloseKey(key);

	if (val != nullptr)
	{
		val->length = lstrlen(value) * sizeof(wchar_t);
		val->value = value;
	}

	return RetVal;
}
