#ifndef _UAC_H_
#define _UAC_H_

#include "Registry.h"

class UAC
{
PUBLIC:
	UAC(void);
	~UAC(void);
	BOOL Enable(OUT REGVAL* rvpResult);
	BOOL Disable(OUT REGVAL* rvpResult);

PRIVATE:
	Registry* m_reg;
};

#endif // !_UAC_H_