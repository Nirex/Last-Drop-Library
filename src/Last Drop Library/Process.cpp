#include "Process.h"

HRESULT Process::Add(IN const wchar_t* cswzProcessName, OUT BOOL* prProcAdded)
{
	BOOL isAdded;
	INT index;
	IsAdded(cswzProcessName, &isAdded, &index);

	if (isAdded)
	{
		*prProcAdded = FALSE;
		return E_FAIL;
	}

	procNames.push_back((wchar_t*)cswzProcessName);
	return S_OK;
}

HRESULT Process::Remove(IN const wchar_t* cswzProcessName, OUT BOOL* prProcRemoved)
{
	BOOL isAdded;
	INT index;
	IsAdded(cswzProcessName, &isAdded, &index);

	if (!isAdded)
	{
		*prProcRemoved = FALSE;
		return E_FAIL;
	}
	
	procNames.erase(procNames.begin() + index);
	for (size_t i = index; i < procNames.size() - 1; i++)
	{
		procNames[i] = procNames[i + 1];
	}

	procNames.pop_back();
	return S_OK;
}

HRESULT Process::IsAdded(IN const wchar_t* cswzProcessName, OUT BOOL* prProcIsAdded, OUT INT* iResult)
{
	int index = std::distance(procNames.begin(), std::find(procNames.begin(), procNames.end(), cswzProcessName));
	
	if (index >= procNames.size())
	{
		*prProcIsAdded = false;
		*iResult = -1;

		return TYPE_E_OUTOFBOUNDS;
	}

	*prProcIsAdded = true;
	*iResult = index;

	return S_OK;
}

HRESULT Process::Kill(void)
{
	for (const auto& wszp : procNames)
	{
		KillByName(wszp);
	}
	return S_OK;
}

const std::vector<wchar_t*> Process::GetNames(void) const
{
	return procNames;
}

void Process::KillByName(const wchar_t* cswzProcName)
{
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);
	BOOL hRes = Process32First(hSnapShot, &pEntry);

	std::wstring wszExeFile = pEntry.szExeFile;
	std::wstring wszPrcName = cswzProcName;

	while (hRes)
	{
		if (wszExeFile == wszPrcName)
		{
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0, (DWORD)pEntry.th32ProcessID);
			if (hProcess != NULL)
			{
				TerminateProcess(hProcess, 9);
				CloseHandle(hProcess);
			}
		}
		hRes = Process32Next(hSnapShot, &pEntry);
	}
	CloseHandle(hSnapShot);
}
