#include "Firewall.h"

Firewall::Firewall(void)
{
	Initialize(&fwProfile);
}

Firewall::~Firewall(void)
{
	Cleanup();
}

HRESULT Firewall::Initialize(OUT INetFwProfile** fwProfile)
{
	HRESULT hr = S_OK;
	INetFwMgr* fwMgr = NULL;
	INetFwPolicy* fwPolicy = NULL;

	_ASSERT(fwProfile != NULL);

	*fwProfile = NULL;

	hr = CoCreateInstance(
		__uuidof(NetFwMgr),
		NULL,
		CLSCTX_INPROC_SERVER,
		__uuidof(INetFwMgr),
		(void**)&fwMgr
	);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = fwMgr->get_LocalPolicy(&fwPolicy);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = fwPolicy->get_CurrentProfile(fwProfile);
	if (FAILED(hr))
	{
		return hr;
	}
	
	if (fwPolicy != NULL)
	{
		fwPolicy->Release();
	}

	if (fwMgr != NULL)
	{
		fwMgr->Release();
	}

	return hr;
}

HRESULT Firewall::IsOn(OUT BOOL* fwOn)
{
	HRESULT hr = S_OK;
	VARIANT_BOOL fwEnabled;

	_ASSERT(fwProfile != NULL);
	_ASSERT(fwOn != NULL);

	*fwOn = FALSE;

	hr = fwProfile->get_FirewallEnabled(&fwEnabled);
	if (FAILED(hr))
	{
		return hr;
	}

	if (fwEnabled != VARIANT_FALSE)
	{
		*fwOn = TRUE;
	}

	return hr;
}

HRESULT Firewall::TurnOn()
{
	HRESULT hr = S_OK;
	BOOL fwOn;

	_ASSERT(fwProfile != NULL);

	hr = IsOn(&fwOn);
	if (FAILED(hr))
	{
		return hr;
	}

	if (!fwOn)
	{
		hr = fwProfile->put_FirewallEnabled(VARIANT_TRUE);
		if (FAILED(hr))
		{
			return hr;
		}
	}

	return hr;
}

HRESULT Firewall::TurnOff()
{
	HRESULT hr = S_OK;
	BOOL fwOn;

	_ASSERT(fwProfile != NULL);

	hr = IsOn(&fwOn);
	if (FAILED(hr))
	{
		return hr;
	}

	if (fwOn)
	{
		hr = fwProfile->put_FirewallEnabled(VARIANT_FALSE);
		if (FAILED(hr))
		{
			return hr;
		}
	}

	return hr;
}

HRESULT Firewall::AppIsEnabled(IN const wchar_t* fwProcessImageFileName, OUT BOOL* fwAppEnabled)
{
	HRESULT hr = S_OK;
	BSTR fwBstrProcessImageFileName = NULL;
	VARIANT_BOOL fwEnabled;
	INetFwAuthorizedApplication* fwApp = NULL;
	INetFwAuthorizedApplications* fwApps = NULL;

	_ASSERT(fwProfile != NULL);
	_ASSERT(fwProcessImageFileName != NULL);
	_ASSERT(fwAppEnabled != NULL);

	*fwAppEnabled = FALSE;

	hr = fwProfile->get_AuthorizedApplications(&fwApps);
	if (FAILED(hr))
	{
		SysFreeString(fwBstrProcessImageFileName);
		if (fwApp != NULL)
		{
			fwApp->Release();
		}
		if (fwApps != NULL)
		{
			fwApps->Release();
		}

		return hr;
	}

	fwBstrProcessImageFileName = SysAllocString(fwProcessImageFileName);
	if (fwBstrProcessImageFileName == NULL)
	{
		hr = E_OUTOFMEMORY;

		SysFreeString(fwBstrProcessImageFileName);
		if (fwApp != NULL)
		{
			fwApp->Release();
		}
		if (fwApps != NULL)
		{
			fwApps->Release();
		}

		return hr;
	}

	hr = fwApps->Item(fwBstrProcessImageFileName, &fwApp);
	if (SUCCEEDED(hr))
	{
		hr = fwApp->get_Enabled(&fwEnabled);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrProcessImageFileName);
			if (fwApp != NULL)
			{
				fwApp->Release();
			}
			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		if (fwEnabled != VARIANT_FALSE)
		{
			*fwAppEnabled = TRUE;
		}
	}
	else
	{
		hr = S_OK;
	}

	return hr;
}

HRESULT Firewall::AddApp(IN const wchar_t* fwProcessImageFileName, IN const wchar_t* fwName)
{
	HRESULT hr = S_OK;
	BOOL fwAppEnabled;
	BSTR fwBstrName = NULL;
	BSTR fwBstrProcessImageFileName = NULL;
	INetFwAuthorizedApplication* fwApp = NULL;
	INetFwAuthorizedApplications* fwApps = NULL;

	_ASSERT(fwProfile != NULL);
	_ASSERT(fwProcessImageFileName != NULL);
	_ASSERT(fwName != NULL);

	hr = AppIsEnabled(
		fwProcessImageFileName,
		&fwAppEnabled
	);
	if (FAILED(hr))
	{
		SysFreeString(fwBstrName);
		SysFreeString(fwBstrProcessImageFileName);

		if (fwApp != NULL)
		{
			fwApp->Release();
		}

		if (fwApps != NULL)
		{
			fwApps->Release();
		}

		return hr;
	}

	if (!fwAppEnabled)
	{
		hr = fwProfile->get_AuthorizedApplications(&fwApps);
		if (FAILED(hr))
		{
			// Free the BSTRs.
			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		hr = CoCreateInstance(
			__uuidof(NetFwAuthorizedApplication),
			NULL,
			CLSCTX_INPROC_SERVER,
			__uuidof(INetFwAuthorizedApplication),
			(void**)&fwApp
		);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		fwBstrProcessImageFileName = SysAllocString(fwProcessImageFileName);
		if (fwBstrProcessImageFileName == NULL)
		{
			hr = E_OUTOFMEMORY;

			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		hr = fwApp->put_ProcessImageFileName(fwBstrProcessImageFileName);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		fwBstrName = SysAllocString(fwName);
		if (SysStringLen(fwBstrName) == 0)
		{
			hr = E_OUTOFMEMORY;

			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		hr = fwApp->put_Name(fwBstrName);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}

		hr = fwApps->Add(fwApp);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);
			SysFreeString(fwBstrProcessImageFileName);

			if (fwApp != NULL)
			{
				fwApp->Release();
			}

			if (fwApps != NULL)
			{
				fwApps->Release();
			}

			return hr;
		}
	}

	SysFreeString(fwBstrName);
	SysFreeString(fwBstrProcessImageFileName);

	if (fwApp != NULL)
	{
		fwApp->Release();
	}

	if (fwApps != NULL)
	{
		fwApps->Release();
	}

	return hr;
}

HRESULT Firewall::PortIsEnabled(IN LONG portNumber, IN NET_FW_IP_PROTOCOL ipProtocol, OUT BOOL* fwPortEnabled)
{
	HRESULT hr = S_OK;
	VARIANT_BOOL fwEnabled;
	INetFwOpenPort* fwOpenPort = NULL;
	INetFwOpenPorts* fwOpenPorts = NULL;

	_ASSERT(fwProfile != NULL);
	_ASSERT(fwPortEnabled != NULL);

	*fwPortEnabled = FALSE;

	hr = fwProfile->get_GloballyOpenPorts(&fwOpenPorts);
	if (FAILED(hr))
	{
		if (fwOpenPort != NULL)
		{
			fwOpenPort->Release();
		}

		if (fwOpenPorts != NULL)
		{
			fwOpenPorts->Release();
		}
	}

	hr = fwOpenPorts->Item(portNumber, ipProtocol, &fwOpenPort);
	if (SUCCEEDED(hr))
	{
		hr = fwOpenPort->get_Enabled(&fwEnabled);
		if (FAILED(hr))
		{
			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		if (fwEnabled != VARIANT_FALSE)
		{
			*fwPortEnabled = TRUE;
		}
	}
	else
	{
		hr = S_OK;
	}

	if (fwOpenPort != NULL)
	{
		fwOpenPort->Release();
	}

	if (fwOpenPorts != NULL)
	{
		fwOpenPorts->Release();
	}

	return hr;
}

HRESULT Firewall::PortAdd(IN LONG portNumber, IN NET_FW_IP_PROTOCOL ipProtocol, IN const wchar_t* name)
{
	HRESULT hr = S_OK;
	BOOL fwPortEnabled;
	BSTR fwBstrName = NULL;
	INetFwOpenPort* fwOpenPort = NULL;
	INetFwOpenPorts* fwOpenPorts = NULL;

	_ASSERT(fwProfile != NULL);
	_ASSERT(name != NULL);

	hr = PortIsEnabled(
		portNumber,
		ipProtocol,
		&fwPortEnabled
	);
	if (FAILED(hr))
	{
		SysFreeString(fwBstrName);

		if (fwOpenPort != NULL)
		{
			fwOpenPort->Release();
		}

		if (fwOpenPorts != NULL)
		{
			fwOpenPorts->Release();
		}
	}

	if (!fwPortEnabled)
	{
		hr = fwProfile->get_GloballyOpenPorts(&fwOpenPorts);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		hr = CoCreateInstance(
			__uuidof(NetFwOpenPort),
			NULL,
			CLSCTX_INPROC_SERVER,
			__uuidof(INetFwOpenPort),
			(void**)&fwOpenPort
		);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		hr = fwOpenPort->put_Port(portNumber);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		hr = fwOpenPort->put_Protocol(ipProtocol);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		fwBstrName = SysAllocString(name);
		if (SysStringLen(fwBstrName) == 0)
		{
			hr = E_OUTOFMEMORY;

			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		hr = fwOpenPort->put_Name(fwBstrName);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}

		hr = fwOpenPorts->Add(fwOpenPort);
		if (FAILED(hr))
		{
			SysFreeString(fwBstrName);

			if (fwOpenPort != NULL)
			{
				fwOpenPort->Release();
			}

			if (fwOpenPorts != NULL)
			{
				fwOpenPorts->Release();
			}
		}
	}

	SysFreeString(fwBstrName);

	if (fwOpenPort != NULL)
	{
		fwOpenPort->Release();
	}

	if (fwOpenPorts != NULL)
	{
		fwOpenPorts->Release();
	}

	return hr;
}

void Firewall::Cleanup(void)
{   
	if (fwProfile != NULL)
	{
		fwProfile->Release();
	}
}
