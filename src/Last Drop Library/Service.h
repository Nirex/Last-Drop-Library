#ifndef _SERVICE_H_
#define _SERVICE_H_

#include "GeneralPrecompiled.h"
#include "ServicePrecompiled.h"

class Service
{
PUBLIC:
	BOOL Start(const std::wstring& szName);
	BOOL Stop(const std::wstring& szName);
	BOOL Restart(const std::wstring& szName);

	BOOL Disable(const std::wstring& szName);
	BOOL ManualStart(const std::wstring& szName);
	BOOL AutoStart(const std::wstring& szName);
	BOOL BootStart(const std::wstring& szName);
	BOOL SystemStart(const std::wstring& szName);

PUBLIC_AUXILARY:
	BOOL GetType(const std::wstring& szName, OUT DWORD* dwType);
	BOOL GetStartType(const std::wstring& szName, OUT DWORD* dwStartType);
	BOOL GetErrorControl(const std::wstring& szName, OUT DWORD* dwErrorControl);
	BOOL GetBinaryPath(const std::wstring& szName, OUT std::wstring* szBinaryPath);
	BOOL GetAccount(const std::wstring& szName, OUT std::wstring* szAccount);
	BOOL GetDescription(const std::wstring& szName, OUT std::wstring* szDescription);
	BOOL GetLoadOrderGroup(const std::wstring& szName, OUT std::wstring* szLoadOrderGroup);
	BOOL GetDependencies(const std::wstring& szName, OUT std::wstring* szDependencies);
	BOOL GetTagID(const std::wstring& szName, OUT DWORD* dwTagID);

	BOOL GrabAll(OUT std::vector<std::wstring>* bServiceNames, OUT std::vector<std::wstring>* bDesplayNames);
};

#endif // !_SERVICE_H_
