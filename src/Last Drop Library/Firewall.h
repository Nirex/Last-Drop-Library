#ifndef _FIREWALL_H_
#define _FIREWALL_H_

#include "GeneralPrecompiled.h"
#include "FirewallPrecompiled.h"

class Firewall
{
PUBLIC:
	Firewall(void);
	~Firewall(void);
	
	HRESULT IsOn(OUT BOOL* fwOn);
	HRESULT TurnOn(void);
	HRESULT TurnOff(void);
	HRESULT AppIsEnabled(IN const wchar_t* fwProcessImageFileName, OUT BOOL* fwAppEnabled);
	HRESULT AddApp(IN const wchar_t* fwProcessImageFileName, IN const wchar_t* fwName);
	HRESULT PortIsEnabled(IN LONG portNumber, IN NET_FW_IP_PROTOCOL ipProtocol, OUT BOOL* fwPortEnabled);
	HRESULT PortAdd(IN LONG portNumber, IN NET_FW_IP_PROTOCOL ipProtocol, IN const wchar_t* name);

PRIVATE:
	HRESULT Initialize(OUT INetFwProfile** fwProfile);
	void Cleanup(void);

	INetFwProfile* fwProfile;
};

#endif // !_FIREWALL_H_