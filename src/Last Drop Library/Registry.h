#ifndef _REGISTRY_H_
#define _REGISTRY_H_

#include "GeneralPrecompiled.h"
#include "RegistryPrecompiled.h"

struct REGVAL
{
PUBLIC:
	REGVAL(std::wstring val, DWORD len) : value(val), length(len) {}
	std::wstring value;
	DWORD length;
};

class Registry
{
PUBLIC:
	BOOL Read(std::wstring szSubkey, std::wstring szName, DWORD type, OUT REGVAL* val);
	BOOL Write(std::wstring szSubkey, std::wstring szName, DWORD type, const wchar_t* value, OUT REGVAL* val);
};

#endif // !_REGISTRY_H_
