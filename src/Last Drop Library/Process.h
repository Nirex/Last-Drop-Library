#ifndef _PROCESS_H_
#define _PROCESS_H_

#include "ProcessPrecompiled.h"
#include "GeneralPrecompiled.h"

class Process
{
PUBLIC:
	HRESULT Add(IN const wchar_t* cswzProcessName, OUT BOOL* prProcAdded);
	HRESULT Remove(IN const wchar_t* cswzProcessName, OUT BOOL* prProcRemoved);
	HRESULT IsAdded(IN const wchar_t* cswzProcessName, OUT BOOL* prProcIsAdded, OUT INT* iResult);
	HRESULT Kill(void);
	const std::vector<wchar_t*> GetNames(void) const;

PRIVATE:
	void KillByName(const wchar_t* cswzProcName);

	std::vector<wchar_t*> procNames;
};

#endif // !_PROCESS_H_
