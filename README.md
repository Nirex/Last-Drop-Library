# Last Drop Library (ON HOLD)

A Library to create powerful viruses, mainly ransomware.

Copyright (c) 2018 Nirex

# Welcome

Welcome to Last Drop Library, A buff up to the good ol' Dark Drop Library which was Made in C#.

Due to the limitations of the language(Mainly the ability of people being able to read the entire source code of the assembly with just a simple drag and drop!), I decided it would be best to do things natively; thus, The Last Drop Library.

NOTE:

Use at your own risk, I as the creator and developer of this project take no liability for any damages caused by the use of this library.

# Copyright

Copyright (C) 2018 Nirex

Please read the LICENSE file for License details and terms of use. 

# Used Terms
[ The term LDL is a shorthand for the Last Drop Library and holds no connection with potential owners of registered trademarks or other rights. ]

# Contact

You can contact me at:

Nirex.0@gmail.com
